import { ProposalDetailPage } from './../pages/proposal-detail/proposal-detail';
import { ProposalPage } from './../pages/proposal/proposal';
import { MakeOfferDfPage } from './../pages/make-offer-df/make-offer-df';
import { OpenDfDetailPage } from './../pages/open-df-detail/open-df-detail';
import { OrdersPage } from './../pages/orders-page/orders-page';
import { OrderCheckOut } from './../pages/order-check-out/order-check-out';
import { OrderPage } from './../pages/order/order';
import { ClassesDetailOrder } from './../pages/classes-detail-order/classes-detail-order';
import { TabsComponent } from './../pages/tabs/tabs';
import { MapsPage } from './../pages/maps/maps';
import { JwtHelper } from 'angular2-jwt';
import { ForgotPage } from './../pages/forgot/forgot';
import { NewAccountPage } from './../pages/new-account/new-account';
import { Storage } from '@ionic/storage';
import { User } from './../models/models';
import { ShareService } from './../providers/share-service';
import { OneSignal } from '@ionic-native/onesignal';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from './../pages/login/login';
import { ImageLoaderConfig } from 'ionic-image-loader';
import { DfPage } from '../pages/df/df';
import { OpensDfPage } from '../pages/opens-df/opens-df';
import { ChatPage } from '../pages/chat/chat';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any ;
  user: User;

  pages: Array<{title: string, component: any, permission:number}>;

  constructor(public platform: Platform,private events:Events, private storage:Storage, private jwtHelper:JwtHelper, private shareSvc:ShareService, public statusBar: StatusBar, public splashScreen: SplashScreen,private oneSignal:OneSignal,private imageLoaderConfig: ImageLoaderConfig) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage, permission: 1 },
      { title: 'Lojas Próximas', component:MapsPage, permission: 1}
    ];

    this.user = this.shareSvc.getUser();
    events.subscribe('user:created', (user) => {
      // debugger;
      console.log(user)
      this.user = user;
    });
    imageLoaderConfig.debugMode= true;
    imageLoaderConfig.enableFallbackAsPlaceholder(true);
    imageLoaderConfig.fallbackUrl= 'https://media.giphy.com/media/jAYUbVXgESSti/giphy.gif';
    imageLoaderConfig.fallbackAsPlaceholder= true;

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      console.log('en')


      // OneSignal Code start:
      // Enable to debug issues:
      // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});


      if (this.platform.is('cordova')){

        this.oneSignal.startInit('ead9ed45-5940-46e9-bf77-e77c1d31b51a','653209420364');
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

        this.oneSignal.handleNotificationReceived().subscribe(() => {
        // do something when notification is received
        });

        this.oneSignal.handleNotificationOpened().subscribe((push) => {
          let data = push.notification.payload.additionalData;
          console.log(push)
          if(data.type == 'new_df' && data.id){
            this.nav.push(OpenDfDetailPage,{id:data.id})
          }else if(data.type == 'new_proposal' && data.id){
            this.nav.push(ProposalDetailPage,{id:data.id})
          }
        });

        this.oneSignal.endInit();
      }

      //this.nav.setRoot(NewAccountPage);
      this.splashScreen.hide();
      this.checkPreviousAuthorization();

      });


  }

  checkPreviousAuthorization(): void{
    console.log('entroi')
    this.storage.get("token").then(token => {
      console.log(token)

      if(token != undefined || token != null){
        let info = this.jwtHelper.decodeToken(token);
        this.shareSvc.setDecodedToken(info);
        // console.log(user)
        this.rootPage = HomePage;
        // this.rootPage = MakeOfferDfPage;
        // this.rootPage = DfPage;

        // this.shareSvc.setUser(user);
        // this.user = user;


          // this.menuCtrl.open();
      }else{
        this.rootPage = LoginPage;
      }
    })
  }

  logout(){
    this.storage.clear().then(res =>{
      console.log(res);
      location.reload();
      this.nav.setRoot(LoginPage);
    });
  }
  openPage(page) {
    this.nav.push(page.component);
  }
  openOrderPage(){
    this.nav.push(OpensDfPage)
  }
}
