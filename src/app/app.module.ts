import { ProposalDetailPage } from './../pages/proposal-detail/proposal-detail';
import { ProposalPage } from './../pages/proposal/proposal';
import { ProposalServiceProvider } from './../providers/proposal-service';
import { MakeOfferDfPiecePage } from './../pages/make-offer-df-piece/make-offer-df-piece';
import { UpServiceProvider } from './../providers/up-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MediaAttachment } from './../pages/attachment/attachment';
import { ChatPage } from './../pages/chat/chat';
import { OpenDfDetailPage } from './../pages/open-df-detail/open-df-detail';
import { OpensDfPage } from './../pages/opens-df/opens-df';
// import { MediaCapture } from '@ionic-native/media-capture';
import { OrderFilterPopOver } from './../components/order-filter-pop-over/order-filter-pop-over';
import { PopoverOptions } from './../components/order-options-pop-over/order-options-pop-over';
import { OrderStatus } from './pipes/orderStatus';
import { OrdersPage } from './../pages/orders-page/orders-page';
import { UserService } from './../providers/user-service';
import { CompleteRegister } from './../pages/complete-register/complete-register';
import { OrderCheckOut } from './../pages/order-check-out/order-check-out';
import { OrderService } from './../providers/order-service';
import { CartPopOver } from './../pages/cart-pop-over/cart-pop-over';
import { OrderPage } from './../pages/order/order';
import { AdditionalPicker } from './../components/additional-picker/additional-picker';
import { AdditionalService } from './../providers/additional-service';
import { ParallaxHeader } from './../components/parallax-header/parallax-header';
import { ClassesDetailOrder } from './../pages/classes-detail-order/classes-detail-order';
import { MenuService } from './../providers/menu-service';
import { ClassesDetail } from './../pages/classes-detail/classes-detail';
import { ClassesService } from './../providers/classes-service';
import { FeedService } from './../providers/feed-service';
import { FeedComponent } from './../pages/feed/feed';
import { ClassesComponent } from './../pages/classes/classes';
import { TabsComponent } from './../pages/tabs/tabs';
import { MessageService } from './../providers/alert-service';
import { TargetTypePipe } from './pipes/target-type';
import { SafeHtml } from './pipes/safe-html';
import { ImagePicker } from '@ionic-native/image-picker';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { MetasServiceProvider } from './../providers/metas-service/metas-service';
import { UserlevelService } from './../providers/userlevel-service';
import { Geolocation } from '@ionic-native/geolocation';
import { MapsPage } from './../pages/maps/maps';
import { retirePipe } from './pipes/retire-target-gift';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonRating } from './../components/ion-rating/ion-rating';
import { CheckoutServiceProvider } from './../providers/checkout-service/checkout-service';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { AppSettings } from './appSetings';
import { ShareService } from './../providers/share-service';
import { AuthService } from './../providers/auth-service';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { CurrencyPipe} from '@angular/common';
import { Elastic} from './../components/elastic-textarea/elastic-textarea';
import { FilterPipeModule } from 'ngx-filter-pipe';

import { IonicImageLoader } from 'ionic-image-loader';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from './../pages/login/login';
import { ForgotPage } from './../pages/forgot/forgot';
import { NewAccountPage } from './../pages/new-account/new-account';
import { TargetsAndGifts } from './../pages/targets-and-gifts/targets-and-gifts';
import { TargetsAndGiftsDetail } from './../pages/targets-and-gifts-detail/targets-and-gifts-detail';

import { TargetsAndGiftsService } from './../providers/targets-and-gifts';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Facebook } from '@ionic-native/facebook';
import { OneSignal } from '@ionic-native/onesignal';

import { AuthHttp, AuthConfig, JwtHelper } from 'angular2-jwt';
import { Storage, IonicStorageModule} from '@ionic/storage';
import { DfPage } from '../pages/df/df';
import { DfServiceProvider } from '../providers/df-service/df-service';
import { MomentModule } from 'angular2-moment';
import { MakeOfferDfPage } from '../pages/make-offer-df/make-offer-df';


export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({noJwtError: true}), http, options);
}

export function getAuthHttp(http, storage,auth) {
  return new AuthHttp(new AuthConfig({
    noJwtError: true,
    globalHeaders: [{'Content-Type': 'application/json'}],
    tokenGetter: (() => storage.get('token').then((token: string) => token)),
    noTokenScheme: true
  }), http);
}


@NgModule({
  declarations: [
    MyApp,
    TabsComponent,
    HomePage,
    LoginPage,
    IonRating,
    ForgotPage,
    MapsPage,
    NewAccountPage,
    TargetsAndGifts,
    TargetsAndGiftsDetail,
    retirePipe,
    TargetTypePipe,
    SafeHtml,
    ClassesComponent,
    FeedComponent,
    ClassesDetail,
    ClassesDetailOrder,
    ParallaxHeader,
    AdditionalPicker,
    OrderPage,
    Elastic,
    CartPopOver,
    OrderCheckOut,
    CompleteRegister,
    OrdersPage,
    OrderStatus,
    PopoverOptions,
    OrderFilterPopOver,
    DfPage,
    OpensDfPage,
    OpenDfDetailPage,
    ChatPage,
    MediaAttachment,
    MakeOfferDfPage,
    MakeOfferDfPiecePage,
    ProposalPage,
    ProposalDetailPage
  ],
  imports: [
    BrowserModule,
    FilterPipeModule,
    HttpModule,
    BrowserAnimationsModule,
    IonicStorageModule.forRoot(),
    MomentModule,
    IonicModule.forRoot(MyApp,{
      backButtonText: '',
    }),
    IonicImageLoader.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsComponent,
    HomePage,
    LoginPage,
    NewAccountPage,
    TargetsAndGifts,
    TargetsAndGiftsDetail,
    ForgotPage,
    MapsPage,
    ClassesComponent,
    FeedComponent,
    ClassesDetail,
    ClassesDetailOrder,
    OrderPage,
    CartPopOver,
    OrderCheckOut,
    CompleteRegister,
    OrdersPage,
    PopoverOptions,
    OrderFilterPopOver,
    DfPage,
    OpensDfPage,
    OpenDfDetailPage,
    ChatPage,
    MediaAttachment,
    MakeOfferDfPage,
    MakeOfferDfPiecePage,
    ProposalPage,
    ProposalDetailPage
  ],
  providers: [
    {
      provide: AuthHttp,
      useFactory: getAuthHttp,
      deps: [Http,Storage,AuthService]
    },
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    Facebook,
    AuthService,
    ShareService,
    CheckoutServiceProvider,
    MetasServiceProvider,
    OneSignal,
    TargetsAndGiftsService,
    MessageService,
    JwtHelper,
    ImagePicker,
    Geolocation,
    FileTransfer,
    File,
    Camera,
    // MediaCapture,
    UserlevelService,
    SafeHtml,
    retirePipe,
    FeedService,
    TargetTypePipe,
    ClassesService,
    MenuService,
    AdditionalService,
    OrderService,
    UserService,
    OrderStatus,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DfServiceProvider,
    UpServiceProvider,
    ProposalServiceProvider
  ]
})
export class AppModule {}
