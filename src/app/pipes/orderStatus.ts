import {Pipe} from '@angular/core';

@Pipe({
 name: 'OrderStatus'
})
export class OrderStatus {
  transform(check) {
    if(check == 'started')
      return "Em aberto"
    else if(check == 'finished')
      return 'Finalizado'
    else if(check == 'canceled')
      return 'Cancelado'
}
}


