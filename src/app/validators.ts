import { FormControl, Validator, AbstractControl,FormGroup } from "@angular/forms";


export class EmailValidator {

  static isValid(control: FormControl): any {
    let regExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

    if (!regExp.test(control.value)) {
      return {"invalidEmail": true};
    }
    return null;
  }
}

export class PasswordValidation {

  static matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }
}


export class AdressValidation {

    static checkDelivery(deliveryKey: any, addressKey: any) {
      return (group: FormGroup): {[key: string]: any} => {
        let delivery = group.controls[deliveryKey];
        let address = group.controls[addressKey];
        console.log(address.value)
        if(delivery.value == false){
          address.disable();
          console.log('ento')
          return {
            addressValid: true
          };
        }else if (delivery.value == true && address.value == null ||  delivery.value == true && address.value  == undefined) {
          return {
            addressValid: false
          };
        }
      }
    }
  }

