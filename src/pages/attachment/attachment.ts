import { UpServiceProvider } from './../../providers/up-service';
import { DfServiceProvider } from './../../providers/df-service/df-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ModalController } from 'ionic-angular';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'media-attachment',
  templateUrl: 'attachment.html',
})
export class MediaAttachment {
  private showCamera : false;
  private showGallery : false;
  private showLocation : false;
  constructor(
    private camera:Camera,
    private viewCtrl: ViewController,
    private modelCtrl: ModalController,
    private navParams:NavParams,
    public upService:UpServiceProvider
    // private pictureService: PictureService
  ) {
    this.showCamera = this.navParams.get('camera');
    this.showGallery = this.navParams.get('gallery');
    this.showLocation = this.navParams.get('location');
  }


  getImage(mode){
    var sourceType = mode;
      const options: CameraOptions = {
          quality: 50,
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: sourceType,
          allowEdit: false,
          encodingType: this.camera.EncodingType.JPEG,
          saveToPhotoAlbum: false,
          correctOrientation:true,
          targetWidth: 600,
      };
      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        this.upService.uplodFile(base64Image).then(response =>{
          console.log(response)
          let res:any = response;
          if(res.success)
            this.viewCtrl.dismiss({url:res.url})
        })
        }, (err) => {
        // Handle error
        });
  }
  // sendPicture(): void {
  //   this.pictureService.select().then((file: File) => {
  //     this.viewCtrl.dismiss({
  //       messageType: MessageType.PICTURE,
  //       selectedPicture: file
  //     });
  //   });
  // }

  // sendLocation(): void {
  //   const locationModal = this.modelCtrl.create(NewLocationMessageComponent);
  //   locationModal.onDidDismiss((location) => {
  //     if (!location) {
  //       this.viewCtrl.dismiss();

  //       return;
  //     }

  //     this.viewCtrl.dismiss({
  //       messageType: MessageType.LOCATION,
  //       selectedLocation: location
  //     });
  //   });

  //   locationModal.present();
  // }
}
