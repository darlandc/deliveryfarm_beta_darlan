import { OrderService } from './../../providers/order-service';
import { OrderPage } from './../order/order';
import { HomePage } from './../home/home';
import { ClassesDetailOrder } from './../classes-detail-order/classes-detail-order';
import { Order } from './../../models/models';
import { ShareService } from './../../providers/share-service';
import {
  NavController,
  Events,
  NavParams,
  ViewController,
  AlertController
} from "ionic-angular";
import { Component } from '@angular/core';

@Component({
  selector: 'page-cart-pop-over',
  templateUrl: 'cart-pop-over.html',
})
export class CartPopOver {
  public order:Order;
  constructor(public navCtrl: NavController,public orderSvc:OrderService, public shareSvc:ShareService,public alertCtrl:AlertController,public viewCtrl:ViewController,public events:Events, public navParams: NavParams) {
    this.order = this.navParams.get('order');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPopOver');
  }
  close() {
    this.viewCtrl.dismiss();
  }

  removeProduct(product){
    let alert = this.alertCtrl.create({
      title: 'Confirma remoção',
      message: 'Você deseja remover '+product.menu.name+' ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remover',
          handler: () => {
            this.shareSvc.removeProductOrder(product);
          }
        }
      ]
    });
    alert.present();

    console.log(product)
  }
  showOrder(){
    this.navCtrl.push(OrderPage);
    this.viewCtrl.dismiss();
  }
  editItem(item,index){
    console.log(item)
    this.navCtrl.push(ClassesDetailOrder,{item:item,classe:item.menu.class,index:index,edit:true});
  }

  cleanCart(){
    let alert = this.alertCtrl.create({
      title: 'Confirma exclusão',
      message: 'Você deseja limpar seu carrinho?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remover',
          handler: () => {
            this.orderSvc.getOrders();
            this.shareSvc.removeOrder();
            this.viewCtrl.dismiss();
          }
        }
      ]
    });
    alert.present();
  }


}
