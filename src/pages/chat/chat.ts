import { ProposalServiceProvider } from './../../providers/proposal-service';
import { NavParams } from 'ionic-angular';
import { ShareService } from './../../providers/share-service';
import { MediaAttachment } from './../attachment/attachment';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Chat, Message, MessageType } from './../../models/models';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, PopoverController } from 'ionic-angular';
import * as io from 'socket.io-client';

@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class ChatPage implements OnInit{
    selectedChat: Chat;
    title: string;
    picture: string;
    // messages: Observable<Message[]>;
    message: string = '';
    private socket: io;
    public user;

    private _list: Message[] = [];
    private _messages: BehaviorSubject<Message[]> = new BehaviorSubject([]);
    public df;

    get messages(): Observable<Message[]> {
        return this._messages.asObservable()
    }

    constructor(public navParams: NavParams, private shareSvc: ShareService, private popoverCtrl: PopoverController, public proposalSvc: ProposalServiceProvider,// private pictureService: PictureService,// private media: Media,
    ) {
        this.user = this.shareSvc.getUser();
        this.selectedChat = <Chat>navParams.get('chat');
        this.title = 'this.selectedChat.title';
        this.df = this.navParams.get('df');
        console.log(this.df)
        this.proposalSvc.getProposal(this.df._id).then(res => {
            console.log(res);
            this.connectSocket(res.info._id);
            if(!this.df.client.name && !this.df.client.image){
              this.df.client = res.info.provider;
              // this.df.client.name = res.info.provider.name;
              // this.df.client.image = res.info.provider.image;
            }
        })
    }

    ngOnInit() {
        // let isEven = false;
        // this.messages.push({

        // });
    }

    connectSocket(proposal) {
        this.socket = io.connect("https://deliveryfarm.herokuapp.com", {
            query: 'sender=' + this.user._id,
            reconnection: true,
            reconnectionDelay: 1000,
            reconnectionDelayMax: 30000,
            reconnectionAttempts: 99999
        });

        // Add a connect listener
        var socketio = this.socket;
        var messages = this._messages;
        var list = this._list;
        var userid = this.user._id;

        this.socket.on('connect', function () {
            console.log('Client has connected to the server!');
            // subscribe on proposal (entra na sala da "proposta"(id da proposta) do fornecedor com o cliente pra determinado pedido)
            socketio.emit('subscribe', proposal);
        });
        // Add a connect listener
        this.socket.on('message', function (message) {
            console.log(message);
            console.log('Received a message from the server!', message);
            list.push({
                sender: message.sender,
                content: message.content,
                createdAt: message.createdAt,
                type: MessageType.TEXT,
                ownership: message.sender == userid ? 'mine' : 'other'
            });
            messages.next(list);

            // messages.push(message);
        });
        this.socket.on('allMessages', function (allMessages) {
            console.log('Received all messages from the server!', allMessages);

            for (var i = 0; i < allMessages.length; i++) {
                var msg = allMessages[i];
                list.push({
                    sender: allMessages[i].sender,
                    content: allMessages[i].content,
                    createdAt: allMessages[i].createdAt,
                    type: MessageType.TEXT,
                    ownership: allMessages[i].sender == userid ? 'mine' : 'other'
                });

            }
            messages.next(list);
        });
        this.socket.on('messageError', function (error) {
            console.log('Received a message ERROR from the server!', error);
            // messages.push(error.message);
            list.push({
                sender: "ERROR",
                content: error.message,
                // createdAt: moment().toDate(),
                type: MessageType.TEXT,
                ownership: 'mine'
            });
            messages.next(list);
        });
        // Add a disconnect listener
        this.socket.on('disconnect', function () {
            console.log('The client has disconnected!');
        });


    }

    // startRecord(): void {
    //     // var audio = this._audio;
    //     this.file.createFile(this.file.tempDirectory, 'my_file.m4a', true).then(() => {
    //         let audio = this.media.create(this.file.tempDirectory.replace(/^file:\/\//, '') + 'my_file.m4a');
    //         audio.startRecord();
    //         window.setTimeout(() => {
    //             audio.stopRecord()
    //             audio.play();
    //         }, 10000);
    //     });
    // }

    // sendLocationMessage(location: Location): void {
    //     this._list.push({
    //         _id: '0',
    //         chatId: '1',
    //         // content: this.message,
    //         content: location.lat + "," + location.lng + "," + location.zoom,
    //         createdAt: moment().toDate(),
    //         type: MessageType.LOCATION,
    //         ownership: 'mine'
    //     });
    //     this._messages.next(this._list);
    // }


    showAttachments(gallery, camera, location): void {
        const popover = this.popoverCtrl.create(MediaAttachment, {
            gallery: true,
            camera: true,
            location: true,
            chat: this.selectedChat
        }, {
                cssClass: 'attachments-popover'
            });

        popover.onDidDismiss((params) => {
            if (params) {
                if (params.messageType === MessageType.LOCATION) {
                    const location = params.selectedLocation;
                    // this.sendLocationMessage(location);
                }
                else if (params.messageType === MessageType.PICTURE) {
                    const blob: File = params.selectedPicture;
                    this.sendPictureMessage(blob);
                }
            }
        });

        popover.present();
    }

    sendPictureMessage(blob: File): void {
        // this.pictureService.upload(blob).then((picture) => {
        //     this._list.push({
        //         _id: '0',
        //         chatId: '1',
        //         // content: this.message,
        //         content: picture.url,
        //         createdAt: moment().toDate(),
        //         type: MessageType.PICTURE,
        //         ownership: 'mine'
        //     });
        // });
    }

    // getLocation(locationString: string): Location {
    //     const splitted = locationString.split(',').map(Number);

    //     return <Location>{
    //         lat: splitted[0],
    //         lng: splitted[1],
    //         zoom: Math.min(splitted[2] || 0, 19)
    //     };
    // }

    // showOptions(): void {
    //     const popover = this.popoverCtrl.create(MessagesOptionsComponent, {
    //         chat: this.selectedChat
    //     }, {
    //             cssClass: 'options-popover messages-options-popover'
    //         });

    //     popover.present();
    // }

    onInputKeypress({ keyCode }: KeyboardEvent): void {
        if (keyCode === 13) {
            this.sendTextMessage();
        }
    }

    sendTextMessage(): void {
        // If message was yet to be typed, abort
        if (!this.message) {
            return;
        }
        this.socket.emit('message', this.message);

        this.message = "";
    }

}
