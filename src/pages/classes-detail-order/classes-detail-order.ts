import { OrderPage } from './../order/order';
import { HomePage } from './../home/home';
import { ShareService } from './../../providers/share-service';
import { ProductOrder, Additional } from './../../models/models';
import { AdditionalPicker } from './../../components/additional-picker/additional-picker';
import {
  NavController,
  NavParams,
  ToastController,
  Events,
  ActionSheetController,
  Platform,
  AlertController
} from "ionic-angular";
import { Component } from '@angular/core';

@Component({
  selector: 'page-classes-detail-order',
  templateUrl: 'classes-detail-order.html',
})
export class ClassesDetailOrder {
  public item;
  public classe;
  public additionalChanged :boolean;
  public productOrder:ProductOrder;
  public edit:boolean = false;
  constructor(public navCtrl: NavController,public alertCtrl:AlertController,public actionSheetCtrl: ActionSheetController,public platform: Platform,public shareSvc:ShareService, public navParams: NavParams, public events:Events,public toastCtrl:ToastController) {
    this.classe = this.navParams.get('classe');
    this.productOrder = new ProductOrder();
    this.productOrder.menu = Object.assign({},this.navParams.get('item')) ;
    this.productOrder.additional = new Array();

    if(this.navParams.get('edit') == true){

      let productEdit = this.navParams.get('item');
      console.log(productEdit.additional)
      this.productOrder.menu = productEdit.menu;
      this.productOrder.additional = productEdit.additional;
      this.productOrder.price = productEdit.price;
      this.productOrder.qtd = productEdit.qtd;
      this.productOrder.note = productEdit.note;
      this.edit = true;
    }
    this.additionalChanged = false;
    events.subscribe(this.productOrder.menu._id+'Changed', (res) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.additionalChanged = res;
    });
  }

  order(){

    this.productOrder.price = this.productOrder.menu.price * this.productOrder.qtd ;
    if(this.edit)
      this.shareSvc.editProductOrder(this.productOrder,this.navParams.get('index'))
    else
      this.shareSvc.newProductOrder(this.productOrder);
    this.showActionSheet()
  }

  addQtd(){
    this.productOrder.qtd ++;
    this.showToast();
  }

  removeQtd(){
    if(this.productOrder.qtd > 1 ){
      this.productOrder.qtd --;
      this.showToast();
    }else if(this.productOrder.qtd == 1){
      let alert = this.alertCtrl.create({
      title: 'Confirma remoção',
      message: 'Você deseja remover '+this.productOrder.menu.name+' ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remover',
          handler: () => {
            this.shareSvc.removeProductOrder(this.productOrder);
          }
        }
      ]
    });
    alert.present();
    }
  }
  showActionSheet(){
    let actionSheet = this.actionSheetCtrl.create({
      title: this.productOrder.menu.name+' adicionado ao carrinho. O que deseja fazer agora?',
      buttons: [
        {
          text: 'Continuar comprando',
          icon: !this.platform.is('ios') ? 'add-circle' : null ,
          handler: () => {
            if(!this.edit)
              this.navCtrl.popToRoot();
            else{
              // this.navCtrl.pop();
              this.navCtrl.push(HomePage);
            }
          }
        },{
          text: 'Fechar pedido',
          icon: !this.platform.is('ios') ? 'cart' : null,
          handler: () => {
            this.navCtrl.push(OrderPage)
          }
        }
      ]
    });
    actionSheet.present();
  }
  showToast(){
    if(this.productOrder.note != null && this.productOrder.note != undefined || this.additionalChanged == true){
      let toast = this.toastCtrl.create({
        message: 'Caso deseje pedir outro(a) '+this.productOrder.menu.name+ ' com diferentes adicionais e/ou observações adicione esse pedido ao carrinho e crie outro de acordo com as suas preferências',
        showCloseButton: true,
        closeButtonText: 'Ok',
      });
      toast.present();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClassesDetailOrder');
  }

}
