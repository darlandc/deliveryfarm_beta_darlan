import { ClassesDetailOrder } from './../classes-detail-order/classes-detail-order';
import { MenuService } from './../../providers/menu-service';
import { NavController, NavParams } from 'ionic-angular';
import { Component} from '@angular/core';

@Component({
  selector: 'page-classes-detail',
  templateUrl: 'classes-detail.html',
})
export class ClassesDetail {
  public classe;
  public menus;
  constructor(public navCtrl: NavController, public navParams: NavParams, public menuSvc:MenuService) {
    this.classe = this.navParams.get('classe');


  }

  ngOnInit() {
  }

  ionViewDidLoad() {
    this.menuSvc.getClassDetails(this.classe._id).then(res =>{
      console.log(res)
      this.menus = res;
    })
    console.log('ionViewDidLoad ClassesDetail');
  }

  openClasseToOrder(item){
    this.navCtrl.push(ClassesDetailOrder,{item:item,classe:this.classe});
  }


}
