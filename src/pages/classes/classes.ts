import { ClassesDetail } from './../classes-detail/classes-detail';
import { ClassesService } from './../../providers/classes-service';
import { NavController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'page-classes',
  templateUrl: 'classes.html',
})
export class ClassesComponent {
  public classes : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public classesSvc: ClassesService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Classes');
  }


  ngOnInit() {
    this.classes = this.classesSvc.classes; // subscribe to entire collection
    this.classesSvc.getClasses();    // load all todos
  }
  openClass(classe){
    this.navCtrl.push(ClassesDetail,{classe:classe});
  }
}
