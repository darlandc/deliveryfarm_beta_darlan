import { FormControl, FormArray } from '@angular/forms';
import { DfServiceProvider } from './../../providers/df-service/df-service';
import { MediaAttachment } from './../attachment/attachment';
import { ViewController, Platform, PopoverController } from 'ionic-angular';
import { UserService } from './../../providers/user-service';
import { ShareService } from './../../providers/share-service';
import { User, Address, Piece } from './../../models/models';
import { NavController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { ImagePicker } from '@ionic-native/image-picker';
import {DomSanitizer} from '@angular/platform-browser';
import { Camera, CameraOptions } from '@ionic-native/camera';
// import {  MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture';
@Component({
  selector: 'page-complete-register',
  templateUrl: 'complete-register.html',
})
export class CompleteRegister {
  public user:User;
  public address:Address = new Address();
  public canExit;
  public showAddress = false;
  public submitAttempt:boolean = false;
  public pieceForm;
  public piece = new Piece();

  public img:any;
  public imgName:any;
  public teste:any;
  base64Image:any;
  private edit:boolean = false;
  private pickImage:boolean = false;
  public media = new Array();
  public makingOffer = false;

  constructor(public camera:Camera, private imagePicker: ImagePicker,public dfService:DfServiceProvider,public popoverCtrl:PopoverController ,private sanitizer:DomSanitizer,public navCtrl: NavController,public platform: Platform,public viewCtrl:ViewController,public actionSheetCtrl: ActionSheetController, /*private mediaCapture: MediaCapture,*/public formBuilder:FormBuilder, public navParams: NavParams, public shareSvc:ShareService, public userSvc:UserService) {
    this.user = this.shareSvc.getUser();
    let itemEdit = this.navParams.get('item');

    if(itemEdit){
      this.pieceForm = formBuilder.group({
        name: [itemEdit.name],
        type: [itemEdit.type],
        amount: [itemEdit.amount],
        description: [itemEdit.description],
        observation: [itemEdit.observation],
        // media:[itemEdit.media]
      })
      this.media = itemEdit.media;
    }else{
      this.pieceForm = formBuilder.group({
        name: ["",Validators.required],
        type: ["",Validators.required],
        amount: ["",Validators.required],
        description: ["",Validators.required],
        observation: [""],
        // media: new FormArray([])
        // num: [this.address.num, Validators.required],
        // complement: [this.address.complement,Validators.required],
        // neighborhood: [this.address.neighborhood,Validators.required],
      })
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompleteRegister');
  }


  showAttachments(gallery,camera,location): void {
    this.media.push('https://s3-us-west-2.amazonaws.com/bot-deixa-que-falo-staging/produtos/1517357369828fileName.jpeg');
    const popover = this.popoverCtrl.create(MediaAttachment,{
      gallery:true,
      camera:true,
      location:false,
    }, {
      cssClass: 'attachments-popover'
    });

    popover.onDidDismiss((params) => {
        if (params) {
          this.media.push(params.url)
          // this.pieceForm.controls['media'].push(params.url);
        }
    });

    popover.present();
}

  editAddress(adress){
    this.showAddress = true;
    this.address = adress;
  }

  newAddress(){
    this.showAddress = true;
  }

  checkForm(){
     if(this.pieceForm.valid){
      let piece = this.pieceForm.value;
      piece.media = this.media;
      this.viewCtrl.dismiss({piece:piece});
     }else
      this.submitAttempt = true;
  }

  // showOptionsAttachment(){
  //   let actionSheet = this.actionSheetCtrl.create({
  //     title: 'Anexar',
  //     buttons: [
  //       {
  //         text: 'Imagem da galeria',
  //         icon: !this.platform.is('ios') ? 'image' : null,
  //         role: 'destructive',
  //         handler: () => {
  //           this.getImage(0);
  //           // this.getImageFromAlbum();
  //         }
  //       },{
  //         text: 'Câmera',
  //         icon: !this.platform.is('ios') ? 'camera' : null,
  //         handler: () => {
  //           this.getImage(1);
  //           // this.getImageFromCamera();
  //         }
  //       }
  //     ]
  //   });
  //   actionSheet.present();
  // }

  // getImageFromAlbum(){
  //   let options = {
  //     maximumImagesCount: 1,
  //     width: 600,
  //     quality: 70,
  //     outputType:1
  //   };
  //   this.imagePicker.getPictures(options).then((results) => {
  //     console.log(results)
  //     this.img = results[0];
  //     this.imgName = this.img.split("/").pop();
  //     let base64Image = 'data:image/jpeg;base64,' + this.img;
  //     this.pickImage = true;
  //     this.dfService.createDf(base64Image);
  //   }, (err) => { });
  // }



  getImageFromCamera(){

  }

  // getImage(mode){
  //   var sourceType = mode;
  //     const options: CameraOptions = {
  //         quality: 50,
  //         destinationType: this.camera.DestinationType.DATA_URL,
  //         sourceType: sourceType,
  //         allowEdit: false,
  //         encodingType: this.camera.EncodingType.JPEG,
  //         saveToPhotoAlbum: false,
  //         correctOrientation:true,
  //         targetWidth: 600,
  //     };
  //     this.camera.getPicture(options).then((imageData) => {
  //       // imageData is either a base64 encoded string or a file URI
  //       // If it's base64:
  //       let base64Image = 'data:image/jpeg;base64,' + imageData;
  //       this.dfService.createDf(base64Image);
  //       }, (err) => {
  //       // Handle error
  //       });
  // }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
