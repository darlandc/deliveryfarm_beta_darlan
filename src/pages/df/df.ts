import { DfServiceProvider } from './../../providers/df-service/df-service';
import { ShareService } from './../../providers/share-service';
import { CompleteRegister } from './../complete-register/complete-register';
import { ModalController, ToastController } from 'ionic-angular';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { DF } from '../../models/models';

@Component({
  selector: 'page-df',
  templateUrl: 'df.html',
})
export class DfPage {
  public itens = [];
  public df = new DF();
  public user;
  constructor(public navCtrl: NavController, public viewCtrl:ViewController, public dfService:DfServiceProvider,public modalCtrl:ModalController,public toastCtrl:ToastController, public shareSvc:ShareService) {
    this.user = this.shareSvc.getUser();
  }

  toggleDetails(data) {
    if (data.showDetails) {
        data.showDetails = false;
        data.icon = 'arrow-dropdown';
    } else {
        data.showDetails = true;
        data.icon = 'arrow-dropup';
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DfPage');
  }

  addPeace(){
    let modal = this.modalCtrl.create(CompleteRegister,{enableBackdropDismiss:false});
    modal.onDidDismiss(data => {
      if (data){
        this.df.itens.push(data.piece)
        console.log(data.piece)
      }
    });
    modal.present();
  }

  editPiece(item){
    let modal = this.modalCtrl.create(CompleteRegister,{item:item},{enableBackdropDismiss:false});
    modal.onDidDismiss(data => {
      if (data){
        let i = this.itens.indexOf(item);
        this.df.itens[i]= data.piece;
      }
    });
    modal.present();
  }


  makeDf(){
    console.log(this.df)
    // this.df.itens = this.itens;
    this.dfService.sendOrder(this.df).then(res =>{
      if(res.success){
        let toast = this.toastCtrl.create({
          message: 'Sua DF solicitada com sucesso. Aguarde o contato de um fornecedor 😀',
          position: 'bottom',
          showCloseButton:true,
          closeButtonText:'OK'
        });

        toast.onDidDismiss(() => {
          this.viewCtrl.dismiss();
        });

        toast.present();
      }else{
        let toast = this.toastCtrl.create({
          message: 'Ocorreu um erro',
          position: 'bottom',
          showCloseButton:true,
          closeButtonText:'OK'
        });
        toast.present();
      }

    })

  }


}
