import { Feed } from './../../models/models';
import { FeedService } from './../../providers/feed-service';
import { NavController, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})
export class FeedComponent {
  public feeds : any;
  public showFeed = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public feedSvc: FeedService) {
    this.feeds = new Array();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Feed');
  }

  ngOnInit() {
    this.feeds = this.feedSvc.feeds; // subscribe to entire collection
    this.feedSvc.getFeeds();    // load all todos

    this.feeds.subscribe(res =>{
      console.log(res)
      this.feeds = res;
      if(this.feeds != undefined)
        this.showFeed = true;
    });
  }

}
