import { Validators, FormBuilder } from '@angular/forms';
import { EmailValidator, PasswordValidation } from './../../app/validators';
import { MessageService } from './../../providers/alert-service';
import { User } from './../../models/models';
import { AuthService } from './../../providers/auth-service';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
})
export class ForgotPage {
  public email:String;
  public user:User;
  formForgot;
  submitAttempt:boolean = false;

  constructor(public navCtrl: NavController, public messageSvc:MessageService,public navParams: NavParams,public toastCtrl:ToastController, public authSvc:AuthService, public alertCtrl:AlertController,private formBuilder:FormBuilder) {
    this.user = new User(null,null,null);
    this.formForgot = formBuilder.group({
      email: ['', EmailValidator.isValid],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Forgot');
  }

  sendForgot(){
    this.authSvc.forgotAccount(this.user).subscribe(
      data => {
        if(data.success)
          console.log('foi')
          // this.messageSvc.showToast('Submissão feita com sucesso');
        else
          console.log('jhh')
          // this.alertSvc.showAlert('Erro',data.message,null);
      },error => {
        console.log('deu ruim');
        // this.alertSvc.showAlert('Erro',error,null);
        console.log(error);
      })
  }

}
