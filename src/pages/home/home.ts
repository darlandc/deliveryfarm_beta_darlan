import { ProposalPage } from './../proposal/proposal';
import { OpensDfPage } from './../opens-df/opens-df';
import { DfPage } from './../df/df';
import { CartPopOver } from './../cart-pop-over/cart-pop-over';
import { FeedComponent } from './../feed/feed';
import { ClassesComponent } from './../classes/classes';
import { TabsComponent } from './../tabs/tabs';
import { UserlevelService } from './../../providers/userlevel-service';
import { TargetsAndGiftsService } from './../../providers/targets-and-gifts';
import { TargetsAndGifts } from './../targets-and-gifts/targets-and-gifts';
import { ShareService } from './../../providers/share-service';
import { User, UserInfo } from './../../models/models';
import { AuthService } from './../../providers/auth-service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Component,ViewChild } from '@angular/core';
import { NavController, Events, AlertController, PopoverController, LoadingController } from 'ionic-angular';
import { ProposalServiceProvider } from '../../providers/proposal-service';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  op: string = "targets";
  user:User;
  info:any;
  pedidos = 0;
  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController,public proposalSvc:ProposalServiceProvider, public popOverCtrl:PopoverController,public alertCtrl:AlertController,private shareSvc:ShareService,public events:Events,private auth:AuthService, private scanner:BarcodeScanner) {
    this.info = {};
    this.info.checkout = 0;
    this.info.qtdgasol = 0;
    this.info.spend = 0;
    this.info = new UserInfo();
    this.user = this.shareSvc.getUser();
    let loading = this.loadingCtrl.create({
      content: 'Carregando'
    });

    loading.present();
    events.subscribe('user:created', (user) => {
      // debugger;
      console.log(user)
      this.user = user;
    });

    events.subscribe('OrderChanged', (order) => {
      console.log(order)
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome order '+ order);
    });
    this.proposalSvc.getProposals().then(res => {
      console.log(res.info)
      this.pedidos = res.info.length;
      loading.dismiss()
      // this.proposals = res.info;
    })
  }

  showCart(myEvent){
    console.log(this.shareSvc.getOrder())
    let popover = this.popOverCtrl.create(CartPopOver,{order:this.shareSvc.getOrder()});
    popover.present({
      ev: myEvent
    });
  }

  openPage(page){
    if (page == 1)
      this.navCtrl.push(DfPage)
    else if (page == 2)
      this.navCtrl.push(OpensDfPage)
    else if(page == 3)
      this.navCtrl.push(ProposalPage)

  }
  // doRefresh(refresher) {
  //   this.
  //   // this.targetAndGiftSvc.getTargets();
  //   // setTimeout(() => {
  //   //   console.log('Async operation has ended');
  //   //   refresher.complete();
  //   // }, 3000);
  // }





}
