import { JwtHelper } from 'angular2-jwt';
import { TabsComponent } from './../tabs/tabs';
import { ShareService } from './../../providers/share-service';
import { EmailValidator } from './../../app/validators';
import { ForgotPage } from './../forgot/forgot';
import { NewAccountPage } from './../new-account/new-account';
import { HomePage } from './../home/home';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './../../providers/auth-service';
import { Facebook } from '@ionic-native/facebook';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams, AlertController, LoadingController, Loading, MenuController } from 'ionic-angular';
import { Component, trigger, state, style, transition, animate, keyframes, } from '@angular/core';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
   animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0'}),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0)'}),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({transform: 'translate3d(0,2000px,0)', offset: 0}),
          style({transform: 'translate3d(0,-20px,0)', offset: 0.9}),
          style({transform: 'translate3d(0,0,0)', offset: 1})
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0}),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class LoginPage {
  logoPrincipal: any = "in";
  logoFundo: any = "in";
  loginState: any = "in";
  formState: any = "in";
  formLogin : any;
  submitAttempt:boolean = false;

  constructor(public navCtrl: NavController, public jwtHelper:JwtHelper , public shareSvc: ShareService, private auth:AuthService,private formBuilder:FormBuilder, private alertCtrl:AlertController, public navParams: NavParams,private facebook:Facebook) {
    this.formLogin = formBuilder.group({
      email: ['', EmailValidator.isValid],
      password: ['',Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  facebookLogin(){
    this.facebook.login(['email','public_profile']).then( (response) => {
      console.log(response)

      if(response.status == "connected"){
        this.auth.loginFB(response.authResponse.accessToken).subscribe(
          data =>{
            console.log(data)
            if(data.success){
              this.auth.setToken(data.token);
              this.setHomePage();
            }else{
              this.errorMessage(data.message);
            }
          },error => {
            console.log('deu ruim');
            console.log(error);
            return Observable.throw(error);
          }
        )
      }
    }).catch((error) => { console.log(error) });
  }

  setHomePage(){
    this.navCtrl.setRoot(HomePage);
  }

  login(){
    if(!this.formLogin.valid)
      this.submitAttempt = true;
    else{
      this.auth.login(this.formLogin.value).subscribe(
        data => {
          console.log(data)
          if(data.success){
            this.setHomePage();
            let decodedToken = this.jwtHelper.decodeToken(data.token);
            this.shareSvc.setDecodedToken(decodedToken);
            this.auth.setToken(data.token);
          }else
            this.errorMessage(data.message);
        },error => {
          console.log('deu ruim');
          console.log(error);
          return Observable.throw(error);
        }
      );
    }

  }

  createNewAccount(){
    this.navCtrl.push(NewAccountPage);
  }

  forgotPassword(){
    this.navCtrl.push(ForgotPage);
  }
  errorMessage(message){
    let alert = this.alertCtrl.create({
      title: 'Erro',
      subTitle: message,
      buttons: ['Ok']
    });
    alert.present();
  }

}
