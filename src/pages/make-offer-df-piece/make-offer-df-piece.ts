import { Validators, FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-make-offer-df-piece',
  templateUrl: 'make-offer-df-piece.html',
})
export class MakeOfferDfPiecePage {
  public item;
  public pieceForm;
  public submitAttempt:boolean = false;
  public edit:boolean = false;

  constructor(public navCtrl: NavController,public viewCtrl:ViewController, public navParams: NavParams, public formBuilder:FormBuilder,) {
    this.item = this.navParams.get('item');
    this.edit = this.navParams.get('edit');
    console.log(this.item)
    if(!this.edit){
      this.pieceForm = formBuilder.group({
        name: ["",Validators.required],
        price: ["",Validators.required],
        amount: ["",Validators.required],
      })
    }else{
      this.pieceForm = formBuilder.group({
        name: [this.item.proposal.name],
        price: [this.item.proposal.price],
        amount: [this.item.proposal.amount],
      })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MakeOfferDfPiecePage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  checkForm(){
    if(this.pieceForm.valid){
    let piece = this.pieceForm.value;
    console.log(piece)
    if(!this.edit)
      piece.orderItem = this.item._id;
    this.viewCtrl.dismiss({piece:piece});
    }else
     this.submitAttempt = true;
  }

}
