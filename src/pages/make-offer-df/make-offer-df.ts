import { ModalController, ToastController, LoadingController, ViewController } from 'ionic-angular';
import { DfServiceProvider } from './../../providers/df-service/df-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MakeOfferDfPiecePage } from '../make-offer-df-piece/make-offer-df-piece';
import { ProposalServiceProvider } from '../../providers/proposal-service';

@Component({
  selector: 'page-make-offer-df',
  templateUrl: 'make-offer-df.html',
})
export class MakeOfferDfPage {
  public df;
  public proposal= new Array();
  constructor(public navCtrl: NavController,public viewCtrl:ViewController,public loadingCtrl:LoadingController,public toastCtrl:ToastController,public proposalSvc:ProposalServiceProvider,public modalCtrl:ModalController, public navParams: NavParams,public dfService:DfServiceProvider) {
    this.df = this.navParams.get('df');
    // this.dfService.getDf('5a723148cc397f0020718205').then(res =>{
    //   console.log(res);
    //   this.df = res;
    // })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MakeOfferDfPage');
  }
  itemChanged(df){
    let modal = this.modalCtrl.create(MakeOfferDfPiecePage,{item:df,makingOffer:true},{enableBackdropDismiss:true});
    modal.onDidDismiss(data => {
      console.log(data)
      let index = this.df.itens.indexOf(df);
      if(data){
        // this.proposal.push(data.piece);
        this.df.itens[index].check = true;
        this.df.itens[index].proposal = data.piece; //guia para editar ou remover
      }else{
        console.log('en')
        this.df.itens[index].check = false;
      }
    });
    modal.present();
    // let item = df;
    // item.price = 0;

    // let index = this.itensPicked.indexOf(df);
    // if(index == -1){
    //   this.itensPicked.push(item);
    // }else{
    //   this.itensPicked.splice(index)
    // }
    // console.log(this.itensPicked)
  }

  editPiece(item){

  }

  sendOffer(){
    for(let i=0, tam = this.df.itens.length; i<tam; i++){
      if(this.df.itens[i].proposal != null && this.df.itens[i].proposal != undefined )
        this.proposal.push(this.df.itens[i].proposal)
      console.log(this.proposal);
    }
    let loading = this.loadingCtrl.create({
      content: 'Carregando'
    });

    loading.present();
    console.log(this.proposal);
    let proposalItem
    this.proposalSvc.sendProposal(this.df._id,this.proposal).then(res => {
      loading.dismiss();
      console.log(res)
      if(res.success){
        let toast = this.toastCtrl.create({
          message: 'Sua proposta foi realizada com sucesso. Aguarde a resposta do produtor 😀',
          position: 'bottom',
          showCloseButton:true,
          closeButtonText:'OK'
        });

        toast.onDidDismiss(() => {
          this.navCtrl.pop();
        });

        toast.present();
      }else{
        let toast = this.toastCtrl.create({
          message: 'Ocorreu um erro',
          position: 'bottom',
          showCloseButton:true,
          closeButtonText:'OK'
        });
        toast.present();
      }
    })
  }

  editItem(item){
    let modal = this.modalCtrl.create(MakeOfferDfPiecePage,{item:item,edit:true},{enableBackdropDismiss:true});
    modal.onDidDismiss(data => {
      console.log(data)
      let index = this.df.itens.indexOf(item);
      if(data)
        this.df.itens[index].proposal = data.piece;
    });
    modal.present();
  }

  removeItem(item){
    let index = this.df.itens.indexOf(item);
    this.df.itens[index].proposal = null;
    this.df.itens[index].check = false;
    console.log(this.df.itens);

    // let index = this.df.itens.indexOf(item);
    // this.df.itens[index].check = false;
    // let i = 0;

    // console.log(this.proposal);
    // for(let i=0, tam = this.proposal.length; i<tam; i++){
    //   if(this.proposal[i].orderItem == item._id )
    //     this.proposal.splice(i);
    // }
    // console.log(this.proposal)
    // console.log(item)
  }

}
