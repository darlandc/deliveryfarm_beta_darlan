import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;

@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})
export class MapsPage {



  @ViewChild('map') mapElement: ElementRef;
  map: any;

	constructor(public navCtrl: NavController, public geolocation: Geolocation) {

	}

    ionViewDidLoad(){
    	this.loadMap();

    }

	loadMap(){
    let latLng = new google.maps.LatLng(-32.1074854, -52.147094);

            let mapOptions = {
              center: latLng,
              zoom: 12,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
            this.addMarker();


	    // this.geolocation.getCurrentPosition().then((position) => {
      //   console.log(position)
	    //   let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

	    //   let mapOptions = {
	    //     center: latLng,
	    //     zoom: 14,
	    //     mapTypeId: google.maps.MapTypeId.ROADMAP
	    //   }

	    //   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
	    //   this.addMarker();

	    // }, (err) => {
	    //   console.log(err);
	    // });

	}

    addMarker(){



        var dataLocalization = [
        							['Buffon Cassino', "Av. Santos Dumont, 46 - Vila Juncao, Rio Grande - RS", "96202-090", "(53) 3234-3150", "Atendimento 24 horas",  -32.0544306, -52.127473, "img/posto.jpg"],
      			  					['Comercial Buffon Combustíveis e Transportes', "Rodovia BR 392 km 18,5, s/n - Parque M.nha, Rio Grande - RS, 96215-840", "(53) 3234-3350", "", -32.0887224, -52.1801381, "img/posto.jpg"],
      			  					['Comercial Buffon Combustiveis e Transportes ltda.', "Av. Rheingantz, 15 - Parque Res. Coelho, Rio Grande - RS, 96200-000", "(53) 3233-5708", "", -32.0419806, -52.1027088,"img/posto.jpg"],
      			  					['Comercial Buffon Combustíveis e Transportes', "R. Gen. Neto, 514 - Centro, Rio Grande - RS, 96200-010", "(53) 3234-3450", "", -32.0379607, -52.0995502,"img/posto.jpg"],
      			  					['Coml Buffon Comb e Transp LTDA. Posto 04', "Industrial Tamandaré, Rio Grande - RS", "(53) 3234-1280", "Atendimento 24 horas", -32.1159571, -52.1219092,"img/posto.jpg"],
      			  					['Buffon Cassino', "Av. Rio Grande, 512 - Cassino, Rio Grande - RS, 96205-000", "(53) 3236-3017", "Atendimento 24 horas", -32.1767619, -52.167201,"img/posto.jpg"]
      			  				];

      	var image = new google.maps.MarkerImage(
				    "img/buffon-icon.png",
				    new google.maps.Size(71, 71),
				    new google.maps.Point(0, 0),
				    new google.maps.Point(17, 34),
				    new google.maps.Size(25, 25));

	    for (var i = 0; i < dataLocalization.length; i++) {
	      var data = dataLocalization[i];
		  let marker = new google.maps.Marker({
		    map:this.map,
		    icon: image,
		    animation: google.maps.Animation.DROP,
		    position: {lat: data[4], lng: data[5]}
		  });

		  var content = "<h4>" + data[0] + "</h4>" +
		  				"<i class='icon  ion-location'></i> " + data[1] + "<br/>" +
		  				"<i class='icon ion-ios-telephone-outline'></i> " + data[2] + "<br/>" +
		  				"<i class='icon ion-ios-clock-outline'></i> " + data[3] + "" +
		  				"<br/><br/><img width='60%' style='float:center;'' src='" + data[6] + "'>";
		  this.addInfoWindow(marker, content);
	    }



	}

	addInfoWindow(marker, content){

	  let infoWindow = new google.maps.InfoWindow({
	    content: content
	  });

	  google.maps.event.addListener(marker, 'click', () => {
	    infoWindow.open(this.map, marker);
	  });

	}

}
