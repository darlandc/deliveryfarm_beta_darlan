import { HomePage } from './../home/home';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './../../providers/auth-service';
import { EmailValidator,PasswordValidation} from './../../app/validators';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'page-new-account',
  templateUrl: 'new-account.html',
})
export class NewAccountPage {
  formNewAccount;
  submitAttempt:boolean = false;
  constructor(public navCtrl: NavController, public auth: AuthService,public alertCtrl: AlertController, public navParams: NavParams,private formBuilder:FormBuilder) {
    this.formNewAccount = formBuilder.group({
      email: ['', EmailValidator.isValid],
      name: ['',Validators.required],
      // gender: ['',Validators.required],
      // birthday: ['',Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    }, {validator: PasswordValidation.matchingPasswords('password', 'confirmPassword')})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewAccount');
  }
  createAccount(){
    if(!this.formNewAccount.valid)
      this.submitAttempt = true;
    else{
      this.auth.createAccount(this.formNewAccount.value).subscribe(
        data => {
          console.log(data)
          if(data.success){
            this.auth.setToken(data.token);
            this.navCtrl.setRoot(HomePage)
          }else
            this.errorMessage(data.message);
        },error => {
          console.log('deu ruim');
          console.log(error);
          return Observable.throw(error);
        }
      );
    }
  }

  errorMessage(message){
    let alert = this.alertCtrl.create({
      title: 'Erro',
      subTitle: message,
      buttons: ['Ok']
    });
    alert.present();
  }

}
