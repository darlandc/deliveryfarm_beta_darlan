import { MakeOfferDfPage } from './../make-offer-df/make-offer-df';
import { DfServiceProvider } from './../../providers/df-service/df-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ChatPage } from '../chat/chat';

@Component({
  selector: 'page-open-df-detail',
  templateUrl: 'open-df-detail.html',
})
export class OpenDfDetailPage {
  public df={};
  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController ,public dfService:DfServiceProvider, public navParams: NavParams) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando'
    });

    loading.present();
    console.log(this.navParams.get("id"));
    this.dfService.getDf(this.navParams.get("id")).then(res => {
      loading.dismiss();
      console.log(res);
      this.df = res;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpenDfDetailPage');
  }

  openChat(){
    this.navCtrl.push(ChatPage,{df:this.df});
  }
  makeOffer(){
    this.navCtrl.push(MakeOfferDfPage,{df:this.df});
  }

}
