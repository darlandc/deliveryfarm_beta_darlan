import { DfServiceProvider } from './../../providers/df-service/df-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { OpenDfDetailPage } from '../open-df-detail/open-df-detail';

@Component({
  selector: 'page-opens-df',
  templateUrl: 'opens-df.html',
})
export class OpensDfPage {
  public opensDf = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl:LoadingController, public dfService:DfServiceProvider) {
    // this.loadingCtrl.create();
    let loading = this.loadingCtrl.create({
      content: 'Carregando'
    });

    loading.present();
    this.dfService.getOpensDf().then(res =>{
      // this.loadingCtrl
      loading.dismiss();
      console.log(res);
      this.opensDf = res;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpensDfPage');
  }
  openOpenDf(id){
    this.navCtrl.push(OpenDfDetailPage,{id:id})
  }
}
