import { OrdersPage } from './../orders-page/orders-page';
import { CompleteRegister } from './../complete-register/complete-register';
import { OrderService } from './../../providers/order-service';
import { User } from './../../models/models';
import { ShareService } from './../../providers/share-service';
import { NavController, NavParams,ModalController,ToastController } from "ionic-angular";
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'page-order-check-out',
  templateUrl: 'order-check-out.html',
})
export class OrderCheckOut {
  public user:User;
  public order;
  public orderForm;
  public submitAttempt:boolean = false;
  constructor(public navCtrl: NavController,public toastCtrl:ToastController, public formBuilder:FormBuilder, public modalCtrl:ModalController,public navParams: NavParams, public shareSvc:ShareService, public orderSvc:OrderService) {
    this.user = this.shareSvc.getUser();


    this.orderForm = formBuilder.group({
      phone: [this.user.phone,Validators.required],
      address: ['', Validators.required],
      delivery: ['',Validators.required],
    })

    if(this.user.phone == undefined || this.user.address.length == 0){
      console.log('en')
      let modal = this.modalCtrl.create(CompleteRegister,{canExit:false},{enableBackdropDismiss:false});
      modal.present();
    }
    this.order = this.shareSvc.getOrder();
    console.log(this.order);
    console.log(this.user);
  }

  deliveryChange(){
    if(this.order.delivery == false)
      this.orderForm.controls['address'].disable();
    else if(this.order.delivery == true){
      let toast = this.toastCtrl.create({
        message: 'Será acrescido uma pequena taxa de entrega após a confirmação do pedido pelo Burguer House',
        showCloseButton: true,
        closeButtonText: 'Ok',
      });
      toast.present();
      this.orderForm.controls['address'].enable();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderCheckOut');
  }

  editProfile(){
    let modal = this.modalCtrl.create(CompleteRegister,{canExit:true});
    modal.present();
    modal.onDidDismiss(data =>{
      console.log(data)
      if(data != undefined)
        this.order.address = data.address;
    })
  }
  sendOrder(){
    if(!this.orderForm.valid)
      this.submitAttempt = true;
    else{
      let formData = this.orderForm.value;
      this.order.phone = formData.phone;

      this.orderSvc.sendOrder(this.order).then(res =>{
        console.log(res)
        if(res.success){
          this.orderSvc.getOrders();
          this.shareSvc.removeOrder();
          this.navCtrl.push(OrdersPage);
        }
      })
    }
    console.log(this.order)
  }

  // enviar(){
  //   // let prods = this.order.itens;
  //   // for (let index = 0; index < prods.length; index++) {
  //   //   this.order.itens.push({product : prods[index]};)
  //   // }
  //   this.order.delivery = true;
  //   this.order.address = new Address();
  //   this.order.address.name = "Marcos";
  //   this.order.address.complement ="rola";
  //   this.order.address.neighborhood ="cassino";
  //   this.order.address.num = 999;

  //   this.orderSvc.sendOrder(this.order).then(res =>{
  //     console.log(res)
  //   })
  //   console.log(this.order)
  // }

}
