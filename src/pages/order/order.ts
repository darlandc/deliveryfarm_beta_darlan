import { LoginPage } from './../login/login';
import { JwtHelper } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { PopoverOptions } from './../../components/order-options-pop-over/order-options-pop-over';
import { OrderCheckOut } from './../order-check-out/order-check-out';
import { ClassesDetailOrder } from './../classes-detail-order/classes-detail-order';
import { OrderService } from './../../providers/order-service';
import { Order, Address } from './../../models/models';
import { ShareService } from './../../providers/share-service';
import { Component } from '@angular/core';
import {
  NavController,
  NavParams,
  AlertController,
  ViewController,
  PopoverController
} from "ionic-angular";

@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {
  public order : Order = new Order();
  shownGroup = null;
  constructor(public navCtrl: NavController,public popoverCtrl: PopoverController, private storage:Storage,private jwtHelper:JwtHelper,public alertCtrl: AlertController,public shareSvc:ShareService, public navParams: NavParams, public orderSvc:OrderService) {
    console.log(this.shareSvc.getOrder())
    this.order = this.shareSvc.getOrder();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Order');
  }

  addQtd(item){
    item.qtd++;
    item.price += item.menu.price;
    this.shareSvc.checkTotalAndSave()
    console.log(item);
  }

  removeQtd(item){
    if(item.qtd >1 ){
      item.qtd --;
      item.price -= item.menu.price;
      this.shareSvc.checkTotalAndSave()
      console.log(item);
    }else if(item.qtd == 1){
      let alert = this.alertCtrl.create({
        title: 'Confirma remoção',
        message: 'Você deseja remover '+item.menu.name+' ?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Remover',
            handler: () => {
              this.shareSvc.removeProductOrder(item);
            }
          }
        ]
      });
      alert.present();
    }
  }

  checkOut(){
    this.storage.get("token").then(token => {
      console.log(token)

      if(token != undefined || token != null){
        let user = this.jwtHelper.decodeToken(token);
        this.shareSvc.setDecodedToken(user);
        console.log(user._id)
        this.navCtrl.push(OrderCheckOut);
      }else
        this.navCtrl.push(LoginPage)
    })
  }

  editItem(item,index){
    console.log(item)
    this.navCtrl.push(ClassesDetailOrder,{item:item,classe:item.menu.class,index:index,edit:true});
  }

  removeProduct(product){
    let alert = this.alertCtrl.create({
      title: 'Confirma remoção',
      message: 'Você deseja remover '+product.menu.name+' ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remover',
          handler: () => {
            this.shareSvc.removeProductOrder(product);
          }
        }
      ]
    });
    alert.present();

    console.log(product)
  }

  showOptions(myEvent,item,index){
    console.log(myEvent)
    console.log(item)
    console.log(index)
    let popover = this.popoverCtrl.create(PopoverOptions,{item:item,index:index});
    popover.present({
      ev: myEvent
    });
  }

}
