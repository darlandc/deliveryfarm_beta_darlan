import { OrderFilterPopOver } from './../../components/order-filter-pop-over/order-filter-pop-over';
import { ShareService } from './../../providers/share-service';
import { OrderStatus } from './../../app/pipes/orderStatus';
import { OrderService } from './../../providers/order-service';
import { NavController, NavParams, AlertController, PopoverController } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'page-orders-page',
  templateUrl: 'orders-page.html',
})
export class OrdersPage {
  public orders : any;
  public noOrders = false;
  public shownGroup = null;
  public filter = {check: {$or: [0, 1,2,3,4]}};
  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController,public alertCtrl:AlertController,public shareSvc:ShareService,public navParams: NavParams, public orderSvc:OrderService) {
    this.orders = new Array();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad OrdersPage');
  }

  ngOnInit() {
    this.orders = this.orderSvc.orders; // subscribe to entire collection
    this.orderSvc.getOrders();    // load all todos

    this.orders.subscribe(res =>{
      console.log(res)
      this.orders = res;
      this.shownGroup = 0;
      if(res == undefined)
        this.noOrders = true;
    });
  }

    refresh(refresher) {
      this.orderSvc.getOrders();    // load all todos
      refresher.complete();
    }
  toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
  };
  isGroupShown(group) {
      return this.shownGroup === group;
  };

  cancelOrder(order){
    let alert = this.alertCtrl.create({
      title: 'Cancelar Pedido',
      message: 'Você deseja realmente cancelar seu pedido ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Remover',
          handler: () => {
            order.check = 6;
            this.orderSvc.updateOrder(order).then(res => {
              console.log(res)
            })
          }
        }
      ]
    });
    alert.present();
  }

  showFilter(myEvent){
    let popover = this.popoverCtrl.create(OrderFilterPopOver);
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss((data) => {
      if(data == 'openOrders'){
        this.filter = {check: {$or: [0,1,2,3,4]}};
      }else if(data == 'closedOrders'){
        this.filter = {check: {$or: [5,6]}};
      }
    })
  }
}
