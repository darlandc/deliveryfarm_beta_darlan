import { ChatPage } from './../chat/chat';
import { ProposalServiceProvider } from './../../providers/proposal-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';


@Component({
  selector: 'page-proposal-detail',
  templateUrl: 'proposal-detail.html',
})
export class ProposalDetailPage {
  public proposal;
  public id;
  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController, public navParams: NavParams,public proposalSvc:ProposalServiceProvider) {
    this.id = this.navParams.get("id");

    if(this.id){
      let loading = this.loadingCtrl.create({
        content: 'Carregando'
      });

      loading.present();
      this.proposalSvc.getProposal(this.id).then(res => {
        loading.dismiss();
        console.log(res);
        this.proposal = res.info;
      })
    }else
      this.proposal = this.navParams.get("proposal");

    console.log(this.proposal)
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ProposalDetailPage');
  }
  openChat(){
    this.navCtrl.push(ChatPage,{df:this.proposal.order});
  }
  acceptProposal(){
    this.proposalSvc.accetpProposal(this.proposal._id,null).then(res => {
      console.log(res)
      if(res.success)
        this.navCtrl.pop();
    })
  }

}
