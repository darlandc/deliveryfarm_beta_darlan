import { ProposalServiceProvider } from './../../providers/proposal-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProposalDetailPage } from '../proposal-detail/proposal-detail';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@Component({
  selector: 'page-proposal',
  templateUrl: 'proposal.html',
})
export class ProposalPage {
  public proposals;
  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController ,public navParams: NavParams, public proposalSvc:ProposalServiceProvider) {
    let loading = this.loadingCtrl.create({
      content: 'Carregando'
    });

    loading.present();
    this.proposalSvc.getProposals().then(res => {
      loading.dismiss();
      console.log(res.info)
      this.proposals = res.info;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProposalPage');
  }

  openProposal(proposal){
    this.navCtrl.push(ProposalDetailPage,{proposal:proposal});
  }

}
