import { Target } from './../../models/models';
import { retirePipe } from './../../app/pipes/retire-target-gift';
import { NavController, NavParams } from 'ionic-angular';
import { Component,Pipe } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
@Component({
  selector: 'page-targets-and-gifts-detail',
  templateUrl: 'targets-and-gifts-detail.html',
})
export class TargetsAndGiftsDetail {
  private type;
  private target;
  private gift;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.target = new Target();
    this.gift = '';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TargetsAndGiftsDetail');
  }

  ngOnInit(){
    this.type = this.navParams.data.type;
    if(this.type == 'target')
      this.target = this.navParams.data.obj;
    else
      this.gift = this.navParams.data.obj;
    console.log(this.navParams.data);
  }

}
