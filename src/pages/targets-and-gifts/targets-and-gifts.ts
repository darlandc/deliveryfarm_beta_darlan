import { TargetsAndGiftsDetail } from './../targets-and-gifts-detail/targets-and-gifts-detail';
import { TargetsAndGiftsService } from './../../providers/targets-and-gifts';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Component } from '@angular/core';

@Component({
  selector: 'targets-and-gifts',
  templateUrl: 'targets-and-gifts.html',
})
export class TargetsAndGifts {
  op: string = "targets";
  private targetsAndGifts;
  public targets = [];
  public gifts = [];


  constructor(public navCtrl: NavController,public events:Events, public navParams: NavParams, public targetAndGiftSvc:TargetsAndGiftsService) {
    events.subscribe('targetsAndGifts:refresh',ref => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log(ref);
      this.targetAndGiftSvc.getTargets();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TargetsAndGifts');
  }

  ngOnInit() {
    this.targetsAndGifts = this.targetAndGiftSvc.targets; // subscribe to entire collection
    this.targetAndGiftSvc.getTargets();    // load all todos

    this.targetsAndGifts.subscribe(res =>{
      console.log(res)
      this.targets = res.targets;
      this.gifts = res.gifts;
    });
  }

  openTargetDetail(target){
    let param = {
      type: 'target',
      obj: target
    }
    this.navCtrl.push(TargetsAndGiftsDetail,param);
  }

  openGiftDetail(gift){
    let param = {
      type: 'gift',
      obj: gift
    }
    this.navCtrl.push(TargetsAndGiftsDetail,param);
  }
}
