import { Additional } from './../models/models';
import { AppSettings } from './../app/appSetings';
import { LoadingController } from 'ionic-angular';
import { AuthConfig, AuthHttp } from 'angular2-jwt';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the AdditionalService provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AdditionalService {

  items: Observable<Additional[]>
  private _items: BehaviorSubject<Additional[]>;
  private dataStore: {
    items: Additional[]
  };
  public data:Additional;

  constructor(public http: Http,public authHttp:AuthHttp, public loadingCtrl:LoadingController) {
    this.dataStore = { items: [] };
    this._items = <BehaviorSubject<Additional[]>>new BehaviorSubject([]);
    this.items = this._items.asObservable();
  }


  getAdditionals() {
    return new Promise(resolve => {
      if(this.dataStore.items.length != 0){
        console.log(this.dataStore.items)
        resolve(this.creatAmount(this.dataStore.items))
      }
      else{
        this.authHttp.get(AppSettings.API_ENDPOINT+'/additional/').map(response => response.json()).subscribe(data => {
          console.log(data)
          resolve(data.info)
          // this.dataStore.items = data.info;
          this.dataStore.items = this.creatAmount(data.info);
          this._items.next(Object.assign({}, this.dataStore).items);
        }, error => {
          console.log(error)
          console.log('Could not load todos.')
        });
      }
    })
  }

  creatAmount(data) : Additional[]{
    data.forEach(element => {
      element.amount = 0;
    });
    return data;
  }

}
