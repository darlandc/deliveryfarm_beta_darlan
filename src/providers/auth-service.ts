import { JwtHelper } from 'angular2-jwt';
import { AppSettings } from './../app/appSetings';
import { Events, LoadingController } from 'ionic-angular';
import { ShareService } from './share-service';
import { Injectable } from '@angular/core';
import { Http, Headers,RequestOptions,Response,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

import { User } from './../models/models';
/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

@Injectable()
export class AuthService {
  private token: any;
  private optionsHeader;
  private headers: Headers = this.getHeaders();
  // public url = 'https://burgerhouse.herokuapp.com' ;
  public url = AppSettings.API_ENDPOINT ;
  //public url ='https://infinity360br.herokuapp.com';


  private userObserver: any;
  private tokenObserver:any;
  public user: User ;
  public hasToken:any;
  public _user:User;
  public _token:any;
  public loading:any;

  constructor(public http: Http, public storage: Storage,public shareSvc:ShareService, public jwtHelper:JwtHelper ,public events: Events,  private loadingCtrl:LoadingController) {
    console.log(this.url);

    this.loading = this.loadingCtrl.create({
      content: 'Carregando'
    });

    this.tokenObserver = null;
    this.hasToken = Observable.create(observer => {
      this.tokenObserver = observer;
    });
/*    console.log('entrou no service')
    this.storage.get('token').then((value) => {
      console.log('token');
      console.log(value);
      let header = new Headers();
      header.append('Content-Type','application/x-www-form-urlencoded');
      if(value != null ){
        this.setaUser(value);
        this.token = value;
        header.append('authorization',this.token);
      }
      this.optionsHeader = new RequestOptions({ headers: header });
    });    */

  }


  getHeaders(): Headers {
      return new Headers({
          'Content-Type': 'application/json',
      });
  }

  private handleUser(user) {
    this.loading.dismiss();

    return this.userObserver.next(user) ;
  }

  handleError(error) {
    this.loading.dismiss();
    console.log(error);
    return error.json().message || 'Server error, please try again later';
  }

/*updateUser(params) {
  return this.getApiToken().flatMap(data => {
      this.headers.append('authorization', data);
      return this.http.put('https://infinity360br.herokuapp.com/api/users/'+params.id, params ,{ headers : this.headers })
          .map(res =>  res.json())
          .catch(this.handleError);
  });
}*/

  setToken(value){
    console.log(this.user)
    console.log('set token')
    this._token = value;
    this.storage.set('token',value).then(res=>{

      let decodedToken = this.jwtHelper.decodeToken(res);
      this.shareSvc.setDecodedToken(decodedToken);
      // console.log(decodedToken)
      // this.user = new User(decodedToken.name,decodedToken.phone,decodedToken.id);
      // this.events.publish('user:created', this.user);
      // this.shareSvc.setUser(this.user);
      // console.log(res)
    })
  }


  createAccount(params){
    this.loading.present();

    return this.http.post(this.url+'/signupuser', params)
      .map((res: Response) => {
        this.loading.dismiss();
        return res.json();
      });
  }

  forgotAccount(params){
    console.log(params)
    // this.loading.present();

    return this.http.post(this.url+'/forgot', params)
      .map((res: Response) => {
        this.loading.dismiss();
        return res.json();
      });
  }

  login(credentials) {
    console.log(credentials)
    this.loading.present();

    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({ headers: headers });

    // let body = JSON.stringify(credentials);
    let body = new URLSearchParams();
    body.set('email', credentials.email);
    body.set('password', credentials.password);

    return this.http.post(this.url+'/login',body)
      .map((res: Response) => {
        this.loading.dismiss();
        return res.json();
      });
  }

  loginFB(fbToken){
    this.loading.present();

    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({ headers: headers });
    this.loading.dismiss();
    return this.http.post(this.url+'/signupuserfb/'+fbToken,options)
      .map((res: Response) => res.json());
  }




  private getHeader() {
    console.log('entrou no getHeader')
    // create authorization header with jwt token
    let header = new Headers();
      // this.storage.get('token').then((token)=>{
      //   console.log('token no getheade')
      //   console.log(token)
      //   header.append('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
      //   if(token){
      //     header.append('authorization',token);
      //     console.log(header)

      //     return new RequestOptions({ headers: header });
      //   }
      // })
    console.log('to loco msm')
    return null
  }

  /*isLogged(){

  }*/

/*  login(credentials){
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this.http.post('https://staging-gg.herokuapp.com/login', JSON.stringify(credentials), {headers: headers})
          .subscribe(res => {
            let data = res.json();
            resolve(data);
            // this.token = data.token;
            // this.storage.set('token', data.token);
            // resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }*/

  logout(){
    // this.storage.set('token', '');
    // this.storage.set('user', '');
  }

}
