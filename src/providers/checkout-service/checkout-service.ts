import { AppSettings } from './../../app/appSetings';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CheckoutServiceProvider {

	public data: any;

	constructor(public http: Http, public authHttp:AuthHttp) {
		console.log('Hello CheckoutServiceProvider Provider');
	}

	load() {
		if (this.data) {
			return Promise.resolve(this.data);
    }


	  	return new Promise(resolve => {
	    	this.authHttp.get(AppSettings.API_ENDPOINT+'/checkout')
	      		.map(res => res.json())
	      		.subscribe(data => {
	        		this.data = data;
	        	resolve(this.data);
	      		});
	  	});
	}
}
