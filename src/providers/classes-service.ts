import { LoadingController } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './../app/appSetings';
import { Class } from './../models/models';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ClassService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ClassesService {

  classes: Observable<Class[]>
  private _classes: BehaviorSubject<Class[]>;
  private dataStore: {
    classes: Class[]
  };
  public loading:any;
  public data:Class;
  constructor(public http: Http, private authHttp: AuthHttp, public loadingCtrl:LoadingController) {

    this.loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    this.loading.present();

    this.dataStore = { classes: [] };
    this._classes = <BehaviorSubject<Class[]>>new BehaviorSubject([]);
    this.classes = this._classes.asObservable();
  }



  getClasses() {
    console.log('entrou')
    this.authHttp.get(AppSettings.API_ENDPOINT+'/class/').map(response => response.json()).subscribe(data => {
      console.log(data)
      this.loading.dismiss();
      this.dataStore.classes = data.info;
      this._classes.next(Object.assign({}, this.dataStore).classes);
    }, error => {
      console.log(error)
      this.loading.dismiss();
      console.log('Could not load todos.')
    });
  }

}
