import { LoadingController } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './../app/appSetings';
import { Feed } from './../models/models';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the FeedService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class FeedService {

  feeds: Observable<Feed[]>
  private _feeds: BehaviorSubject<Feed[]>;
  private dataStore: {
    feeds: Feed[]
  };
  public loading:any;
  public data:Feed;
  constructor(public http: Http, private authHttp: AuthHttp, public loadingCtrl:LoadingController) {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    this.loading.present();
    console.log('Hello FeedService Provider');

    this.dataStore = { feeds: [] };
    this._feeds = <BehaviorSubject<Feed[]>>new BehaviorSubject([]);
    this.feeds = this._feeds.asObservable();
  }

  getFeeds() {
    console.log('entrou')
    this.authHttp.get(AppSettings.API_ENDPOINT+'/feed/').map(response => response.json()).subscribe(data => {
      console.log(data)
      this.loading.dismiss();
      this.dataStore.feeds = data.info;
      this._feeds.next(Object.assign({}, this.dataStore).feeds);
    }, error => {
      console.log(error)
      this.loading.dismiss();
      console.log('Could not load todos.')
    });
  }
}
