import { LoadingController } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './../app/appSetings';
import { Menu } from './../models/models';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http, RequestOptions,URLSearchParams } from "@angular/http";
import 'rxjs/add/operator/map';

/*
  Generated class for the MenuService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class MenuService {

  menus: Observable<Menu[]>
  private _menus: BehaviorSubject<Menu[]>;
  private dataStore: {
    menus: Menu[]
  };
  public loading:any;
  public data:Menu;
  constructor(public http: Http, private authHttp: AuthHttp, public loadingCtrl:LoadingController) {

    this.loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    this.loading.present();

    this.dataStore = { menus: [] };
    this._menus = <BehaviorSubject<Menu[]>>new BehaviorSubject([]);
    this.menus = this._menus.asObservable();
  }



  getClassDetails(idClass) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('class', idClass);
    return new Promise(resolve => {
      this.authHttp.get(AppSettings.API_ENDPOINT+'/menu/',{search:params}).map(response => response.json()).subscribe(data => {
        let original = Object.assign({}, data.info);
        console.log(original)
        console.log(data.info)
        data.info = this.setPrice(data.info);
        this.loading.dismiss();
        this.dataStore.menus = data.info;
        this._menus.next(Object.assign({}, this.dataStore).menus);
        resolve(data.info);
      }, error => {
        console.log(error)
        this.loading.dismiss();
        console.log('Could not load todos.')
      });
    })
    }

    setPrice(itemsMenu){
      for(let i=0, tam = itemsMenu.length; i<tam; i++){
        let originalPrice = itemsMenu[i].price;
        itemsMenu[i].price = (itemsMenu[i].price -((itemsMenu[i].price*itemsMenu[i].off)/100 ));
        itemsMenu[i].off = originalPrice;
      }
      return itemsMenu;
    }



}


