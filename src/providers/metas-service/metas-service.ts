import { Storage } from '@ionic/storage';
import { FileTransferObject,FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { LoadingController } from 'ionic-angular';
import { Target } from './../../models/models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './../../app/appSetings';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MetasServiceProvider {

  public data: any;
  targets: Observable<any[]>
  private _targets: BehaviorSubject<any[]>;
  private dataStore: {
    targets: any[]
  };
  public loading:any;
  public url = AppSettings.API_ENDPOINT+'/target/';


	constructor(public http: Http,private authHttp: AuthHttp, public loadingCtrl:LoadingController,private transfer: FileTransfer, private file: File, private storage:Storage) {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    this.loading.present();

    this.dataStore = { targets: [] };
    this._targets = <BehaviorSubject<Target[]>>new BehaviorSubject([]);
    this.targets = this._targets.asObservable();
  }

  getTargets() {
    console.log('entrou')
    this.authHttp.get(AppSettings.API_ENDPOINT+'/target/').map(response => response.json()).subscribe(data => {
      console.log(data)
      this.loading.dismiss();
      this.dataStore.targets = data.info;
      this._targets.next(Object.assign({}, this.dataStore).targets);
    }, error => {
      console.log(error)
      this.loading.dismiss();
      console.log('Could not load todos.')
    });
  }

  createTarget(file,fileName,meta){
    const fileTransfer: FileTransferObject = this.transfer.create();

    this.storage.get('token').then(res => {
      let token = res;
      let url = 'http://buffontest.herokuapp.com/api/target';

      var options = {
          fileKey: "image",
          fileName: fileName,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params: meta,
          httpMethod:'POST',
          headers:{
            Authorization: token
          }
      };
      console.log(options)
      fileTransfer.upload(file,url, options).then((results) => {
        let res = JSON.parse(results.response);
        console.log(this.dataStore)
        if(res.success){
          this.dataStore.targets.push(res.target);
          this._targets.next(Object.assign({}, this.dataStore).targets);
        }else{
          alert(res.message);
        }
        console.log(res);
      }, error => {
        console.log(error)
          alert('server error');
      });
    })
  }

  updateTargetNovaImg(file,fileName,meta){
    const fileTransfer: FileTransferObject = this.transfer.create();

        this.storage.get('token').then(res => {
          let token = res;
          let url = 'http://buffontest.herokuapp.com/api/target/'+meta._id;

          var options = {
              fileKey: "image",
              fileName: fileName,
              chunkedMode: false,
              mimeType: "multipart/form-data",
              params: meta,
              httpMethod:'PUT',
              headers:{
                Authorization: token
              }
          };
          console.log(options)
          fileTransfer.upload(file,url, options).then((results) => {
            let res = JSON.parse(results.response);
            console.log(res)
            if(res.success){
              this.dataStore.targets.forEach((t, i) => {
                if (t._id === res.info._id) { this.dataStore.targets[i] = res.info; }
              });

              this._targets.next(Object.assign({}, this.dataStore).targets);
            }else{
              alert(res.message);
            }
            console.log(res);
          }, error => {
            console.log(error)
              alert('server error');
          });
        })
  }

  updateTargetSemImg(meta){
    this.authHttp.put(AppSettings.API_ENDPOINT+'/target/'+meta._id,meta)
    .map(response => response.json()).subscribe(res => {
      this.loading.dismiss();
      debugger;
      console.log(res)
      if(res.success){
        this.dataStore.targets.forEach((t, i) => {
          if (t._id === res.info._id) { this.dataStore.targets[i] = res.info; }
        });

        this._targets.next(Object.assign({}, this.dataStore).targets);
      }
    }, error => console.log('Could not update todo.'));
  }

	load() {
		if (this.data) {
			return Promise.resolve(this.data);
		}

		let headers = new Headers();
		headers.append('authorization', "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU5OTIwYzhiOWE5YzdhM2JiM2RkZTgzMCIsIm5hbWUiOiJ0ZXN0ZSIsInBpY3R1cmUiOiJodHRwczovL2xvY2FsaG9zdC9pbWcvbm9waG90by5wbmciLCJhZG1pbiI6MiwicGVybWlzc2lvbiI6eyJsaW1pdCI6ZmFsc2UsInNlbGVjdCI6W119LCJpYXQiOjE1MDI3NDM5MDAsImV4cCI6MTUwNTMzNTkwMH0.rfhHfotcSSoYjSrAAttDXE8x1PXmo_SgpMC0x0mJXJ8");
		headers.append('Content-Type', 'application/json');


	  	return new Promise(resolve => {
			this.authHttp.get('https://buffontest.herokuapp.com/api/target')
	      		.map(res => res.json())
	      		.subscribe(data => {
	        		this.data = data;
	        	resolve(this.data);
	      		});
	  	});
	}

	delete(id){
		return new Promise(resolve => {
			this.authHttp.delete('https://buffontest.herokuapp.com/api/target/' + id)
					.subscribe(data => {
						console.log(data);
					resolve(this.data);
					});
			});
  }

  deleteTarget(id) {
    this.authHttp.delete(this.url+id)
    .map(response => response.json()).subscribe(data => {
      this.loading.dismiss();
      if(data.success){
        this.dataStore.targets.forEach((t, i) => {
          if (t._id === id) { this.dataStore.targets.splice(i, 1); }
        });

        this._targets.next(Object.assign({}, this.dataStore).targets);
      }
    }, error => console.log('Could not delete todo.'));
  }

	save(item) {
		if (item._id != undefined){
			return new Promise(resolve => {
				this.authHttp.put('https://buffontest.herokuapp.com/api/target/' + item._id, item)
						.subscribe(data => {
							console.log(data);
						resolve(this.data);
						});
				});
		}
		else{
			return new Promise(resolve => {
				this.authHttp.post('https://buffontest.herokuapp.com/api/target', item)
						.subscribe(data => {
							console.log(data);
					resolve(this.data);
						});
				});
		}

	}
}
