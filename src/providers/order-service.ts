import { Order } from './../models/models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { AppSettings } from './../app/appSetings';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the OrderService provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderService {

  orders: Observable<Order[]>
  private _orders: BehaviorSubject<Order[]>;
  private dataStore: {
    orders: Order[]
  };
  public loading:any;
  public data:Order;
  constructor(public http: Http,private authHttp: AuthHttp) {
    console.log("Hello OrderService Provider");


    this.dataStore = { orders: [] };
    this._orders = <BehaviorSubject<Order[]>>new BehaviorSubject([]);
    this.orders = this._orders.asObservable();
  }

  getOrders() {
    console.log('entrou')
    this.authHttp.get(AppSettings.API_ENDPOINT+'/api/order/').map(response => response.json()).subscribe(data => {
      console.log(data)
      this.dataStore.orders = data.info;
      this._orders.next(Object.assign({}, this.dataStore).orders);
    }, error => {
      console.log(error)
      console.log('Could not load todos.')
    });
  }
  sendOrder(data) {
    console.log(data);
    return this.authHttp
      .post(AppSettings.API_ENDPOINT + "/api/order/", data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
    /* .map(response => response.json())
      .subscribe(
        data => {
          this.loading.dismiss();
          console.log(data);
          // if (data.success) {
          //   this.dataStore.targets.forEach((t, i) => {
          //     if (t._id === data.info._id) {
          //       console.log(t);
          //       this.dataStore.targets[i] = data.info;
          //     }
          //   });

          //   this._targets.next(Object.assign({}, this.dataStore).targets);
          // }
        },
        error => console.log("Could not update todo.") */
  }

  updateOrder(data){
    console.log(data);
    return this.authHttp
      .put(AppSettings.API_ENDPOINT + "/api/order/"+data._id, data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }
}
