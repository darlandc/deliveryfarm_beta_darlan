import { AppSettings } from './../app/appSetings';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthHttp } from 'angular2-jwt';

/*
  Generated class for the ProposalServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProposalServiceProvider {

  constructor(public http: Http, public authHttp:AuthHttp) {
    console.log('Hello ProposalServiceProvider Provider');
  }

  sendProposal(id,data) {
    console.log(data);
    return this.authHttp
      .post(AppSettings.API_ENDPOINT + "/proposal/"+id, data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }
  getProposal(id){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/proposal/"+id)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  getProposals(){
    return this.authHttp
      .get(AppSettings.API_ENDPOINT + "/proposal/")
      .toPromise()
      .then(res => res.json(), err => console.log(err));
  }

  accetpProposal(id,data){
    return this.authHttp
    .post(AppSettings.API_ENDPOINT + "/order/accept-proposal/"+id,data)
    .toPromise()
    .then(res => res.json(), err => console.log(err));
  }

}
