import { JwtHelper } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { Events } from 'ionic-angular';
import { User, Order, Address } from './../models/models';
import { OneSignal } from "@ionic-native/onesignal";

export class ShareService {
  admin:string;
  user:User ;
  token:string;
  order:Order;
  public events:Events = new Events();
  public storage= new Storage(null);
  private jwtHelper = new JwtHelper();
  private oneSignal = new OneSignal();
  /*name: string;
  sector: string;
  id: string;
  admin: number;
  /*public user:{
  }*/

  constructor() {
      this.token = '';
      console.log(this.order)
      this.storage.get('order').then(res =>{
        console.log(res)
        if(res == null)
          this.order = new Order();
        else
        this.order = res;
      });
    }

    setToken(token){
      this.storage.set('token',token).then(res => {
        console.log(res);
        let user = this.jwtHelper.decodeToken(token);
        this.setDecodedToken(user);
      })
    }


    setDecodedToken(decodedToken){
      console.log(decodedToken)
      this.user = decodedToken.user;
      this.oneSignal.sendTags({"access":decodedToken.user.access,"name":decodedToken.user.name,"id":decodedToken.user._id,"email":decodedToken.user.email});

      // if(decodedToken.address)
        // this.user.address = decodedToken.address;
      console.log(this.user)
      this.events.publish('user:created', this.user);
      this.setUser(this.user);
    }

    setUser(user) {
      console.log(user)
      this.user = user;
      /*this.user.name = user.name;
      this.user.id = user.id;
      this.user.admin = user.admin;
      this.user.sector = user.sector;*/
    }
    checkTotalAndSave(){
      this.order.total = 0;
      for(let item of this.order.itens) {
        this.order.total += item.price;
      }
      let storage:Storage;
      console.log(this.order.total)
      this.storage.set('order',this.order);
    }
    newProductOrder(product){
      console.log(product)
      // é feita assign do objeto para o objeto "pai" nao sofrer alteração
      let productCloned = Object.assign({},product) ;

      this.order.itens.push(productCloned);
      this.checkTotalAndSave();
      console.log(this.order)
    }

    editProductOrder(product,index){
      console.log(index)
      let productCloned = Object.assign({},product) ;
      this.order.itens[index] = product;
      this.checkTotalAndSave();
    }

    removeProductOrder(product){
      let index = this.order.itens.indexOf(product);
      this.order.itens.splice(index,1);
      this.checkTotalAndSave();
      console.log(this.order.itens)
    }

    removeOrder(){
      this.order = null;
      this.order = new Order();
      this.storage.remove('order');
    }

    getOrder(){
      return this.order;
    }

    getUser() {
      return this.user;
        // return this.user;
    }

    getUserId(){
      return this.user._id;
    }
    getAdmin(){
      return this.admin;
    }

    getToken(){
      return this.token;
    }
}
