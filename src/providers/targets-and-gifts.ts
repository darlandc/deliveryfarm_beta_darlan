import { AppSettings } from './../app/appSetings';
import { LoadingController } from 'ionic-angular';
import { AuthHttp } from 'angular2-jwt';
import { User, Target } from './../models/models';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response, URLSearchParams } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class TargetsAndGiftsService {

  targets: Observable<Target[]>
  private _targets: BehaviorSubject<any[]>;
  private dataStore: {
    targets: Target[]
  };
  public loading:any;
  public info:any;

  constructor(public http: Http,private authHttp: AuthHttp, public loadingCtrl:LoadingController) {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    this.loading.present();

    this.dataStore = { targets: [] };
    this._targets = <BehaviorSubject<User[]>>new BehaviorSubject([]);
    this.targets = this._targets.asObservable();
  }

  getTargets() {
    console.log('entrou')
    this.authHttp.get(AppSettings.API_ENDPOINT+'/usergift/').map(response => response.json()).subscribe(data => {
      console.log(data)
      this.loading.dismiss();
      this.dataStore.targets = data;
      this._targets.next(Object.assign({}, this.dataStore).targets);
    }, error => {
      console.log(error)
      this.loading.dismiss();
      console.log('Could not load todos.')
    });
  }





  getUser(id: number | string) {
    console.log(id)
    this.authHttp.get(AppSettings.API_ENDPOINT+'/targets/'+id).map(response => response.json()).subscribe(data => {
      this.loading.dismiss();
      let notFound = true;

      this.dataStore.targets.forEach((item, index) => {
        if (item._id === data.info._id) {
          this.dataStore.targets[index] = data;
          notFound = false;
        }
      });

      if (notFound) {
        this.dataStore.targets.push(data);
      }

      this._targets.next(Object.assign({}, this.dataStore).targets);
    }, error => console.log('Could not load todo.'));
  }

  updateUser(data) {
    console.log(data)
    this.authHttp.put(AppSettings.API_ENDPOINT+'/targets/'+data._id,data)
      .map(response => response.json()).subscribe(data => {
        this.loading.dismiss();
        console.log(data)
        if(data.success){
          this.dataStore.targets.forEach((t, i) => {
            if (t._id === data.info._id) {
              console.log(t);
              this.dataStore.targets[i] = data.info;
            }
          });

          this._targets.next(Object.assign({}, this.dataStore).targets);
        }
      }, error => console.log('Could not update todo.'));
  }


  sendCheckOut(data) {
    console.log(data);
    return this.authHttp
      .post(AppSettings.API_ENDPOINT + "/readqr/", data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
      /* .map(response => response.json())
      .subscribe(
        data => {
          this.loading.dismiss();
          console.log(data);
          // if (data.success) {
          //   this.dataStore.targets.forEach((t, i) => {
          //     if (t._id === data.info._id) {
          //       console.log(t);
          //       this.dataStore.targets[i] = data.info;
          //     }
          //   });

          //   this._targets.next(Object.assign({}, this.dataStore).targets);
          // }
        },
        error => console.log("Could not update todo.") */
  }

  public handleError(error: Response) {
    this.loading.dismiss();
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
