import { Storage } from '@ionic/storage';
import { AppSettings } from './../app/appSetings';
import { LoadingController } from 'ionic-angular';
import { AuthHttp } from 'angular2-jwt';
import { FileTransferObject, FileTransfer } from '@ionic-native/file-transfer';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UpServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UpServiceProvider {

  constructor(public http: Http,private authHttp: AuthHttp,public storage:Storage, public loadingCtrl:LoadingController,private transfer: FileTransfer) {
    console.log('Hello UpServiceProvider Provider');
  }

  uplodFile(file){
    return new Promise(resolve => {
    const fileTransfer: FileTransferObject = this.transfer.create();

    this.storage.get('token').then(res => {
      let token = res;
      let url = AppSettings.API_ENDPOINT + "/upload/";

      var options = {
          fileKey: "image",
          fileName: 'fileName.jpeg',
          chunkedMode: false,
          mimeType: "multipart/form-data",
          httpMethod:'POST',
          headers:{
            Authorization: token,
            'x-access-token':token
          }
      };

        fileTransfer.upload(file,url, options)
        .then((results) => {
          let res = JSON.parse(results.response);
          resolve(res)
          console.log(res);
        }, error => {
          console.log(error)
            alert('server error');
        });
      })
    })
  }
}
