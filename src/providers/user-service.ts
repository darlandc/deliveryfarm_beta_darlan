import { ShareService } from './share-service';
import { AppSettings } from './../app/appSetings';
import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserService provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserService {

  constructor(public http: Http,private authHttp: AuthHttp,public shareSvc:ShareService) {
    console.log('Hello UserService Provider');
  }

  updateUser(data) {
    console.log(data);
    return this.authHttp
      .put(AppSettings.API_ENDPOINT + "/api/user/"+this.shareSvc.getUserId(), data)
      .toPromise()
      .then(res => res.json(), err => console.log(err));
    /* .map(response => response.json())
      .subscribe(
        data => {
          this.loading.dismiss();
          console.log(data);
          // if (data.success) {
          //   this.dataStore.targets.forEach((t, i) => {
          //     if (t._id === data.info._id) {
          //       console.log(t);
          //       this.dataStore.targets[i] = data.info;
          //     }
          //   });

          //   this._targets.next(Object.assign({}, this.dataStore).targets);
          // }
        },
        error => console.log("Could not update todo.") */
  }
}
