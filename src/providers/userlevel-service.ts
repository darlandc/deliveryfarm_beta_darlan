import { ShareService } from './share-service';
import { AppSettings } from './../app/appSetings';
import { LoadingController } from 'ionic-angular';
import { AuthHttp } from 'angular2-jwt';
import { User, UserInfo } from './../models/models';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response, URLSearchParams } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserlevelService {

  users: Observable<UserInfo[]>
  private _users: BehaviorSubject<UserInfo[]>;
  private dataStore: {
    users: UserInfo[]
  };
  public loading:any;
  public data:UserInfo;


  constructor(public http: Http,private authHttp: AuthHttp, public loadingCtrl:LoadingController, private shareSvc:ShareService) {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando'
    });
    // this.loading.present();

    this.dataStore = { users: [] };
    this._users = <BehaviorSubject<UserInfo[]>>new BehaviorSubject([]);
    this.users = this._users.asObservable();
  }

  getUserInfo(){
    console.log('entrou')
    return new Promise(resolve => {

      this.authHttp.get(AppSettings.API_ENDPOINT+'/usergift/').map(response => response.json()).subscribe(data => {
        console.log(data)
        resolve(data.userllevel)
      }, error => console.log(error));
    })
  }

  getUser(params) {
		if (this.data) {
			return Promise.resolve(this.data);
		}

    return new Promise(resolve => {
			this.authHttp.get(AppSettings.API_ENDPOINT+'/userlevel/')
	      		.map(res => res.json())
	      		.subscribe(data => {
              this.loading.dismiss();
	        		this.data = data.userlevel;
	        	resolve(this.data);
	      		});
      })
  }

  getUsers() {
    console.log('entrou')
    this.authHttp.get(AppSettings.API_ENDPOINT+'/userlevel/').map(response => response.json()).subscribe(data => {
      console.log(data.userlevel)
      this.loading.dismiss();
      this.dataStore.users = data.userlevel;
      this._users.next(Object.assign({}, this.dataStore).users);
    }, error => {
      console.log(error)
      this.loading.dismiss();
      console.log('Could not load todos.')
    });
  }

  getUser2(params) {

    console.log('en')

    this.authHttp.get(AppSettings.API_ENDPOINT+'/userlevel/').map(response => response.json()).subscribe(data => {
      console.log(data)
      this.loading.dismiss();
      let notFound = true;

      this.dataStore.users.forEach((item, index) => {
        this.dataStore.users.push(data);
      })

      this._users.next(Object.assign({}, this.dataStore).users);
    }, error => console.log('Could not load todo.'));
  }


  public handleError(error: Response) {
    this.loading.dismiss();
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
